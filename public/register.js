// Your web app's Firebase configuration

var firebaseConfig = {
    apiKey: "AIzaSyC6Jrt-ERGu6zDjouQzpCZimKQRszyuq2I",
    authDomain: "proyecto-febrero-266e7.firebaseapp.com",
    databaseURL: "https://proyecto-febrero-266e7-default-rtdb.firebaseio.com/",
    projectId: "proyecto-febrero-266e7",
    storageBucket: "gs://proyecto-febrero-266e7.appspot.com",
    messagingSenderId: "194350309764",
    appId: "1:194350309764:web:49de5943f06ec50794bdd1"
  };


// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var boton_registrarse = document.getElementById("boton_registrarse");
boton_registrarse.addEventListener('click', registrar, false);


function registrar(){
    var email = document.getElementById('email').value;
    var contrasena = document.getElementById('contrasena').value;
    var rcontrasena = document.getElementById('rcontrasena').value;
    var inputUsername = document.getElementById('nombre').value;
    var ciudad = document.getElementById('ciudad').value;
   
    
    if(email =='' || contrasena =='' || rcontrasena =='' || inputUsername =='' || ciudad ==''){
        alert('Complete todos los campos por favor.')
        return
    }
    var apellido=inputUsername.split(' ')[1];
    if(apellido == null){
        alert('falta apellido')
        return
    }
    var extra=inputUsername.split(' ')[2];
    if(extra !=null){
        alert('coloque solo primer nombre y primer apellido')
        return
    }


    if(contrasena != rcontrasena){
        alert('Las contraseñas no son iguales.')
        return
    }
    
    firebase.auth().createUserWithEmailAndPassword(email, contrasena)
        .then(function (){
            console.log('La cuenta fue creada exitosamente :).')
            firebase.auth().signInWithEmailAndPassword(email, contrasena)
                .then(function(){
                    var userId = firebase.auth().currentUser.uid;
                    console.log(userId);
                    firebase.database().ref('Datos_Usuarios/' + userId + '/').push({  
                        email : email,
                        inputUsername : inputUsername,
                        ciudad : ciudad,       
                    }).then(function(){
                        window.location.replace("./login.html");
                    });
                   
                });
            
        })
        .catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;

            switch (errorCode) {
                case 'auth/weak-password':
                    alert('La contraseña debe tener al menos 6 caracteres.');
                    break;

                case 'auth/email-already-in-use':
                    alert('El correo ya está siendo utilizado.');
                    break;

                case 'auth/invalid-email':
                    alert('Por favor ingrese un correo valido.');
                    break;

                default:
                    alert(errorMessage);
                    break;
            }

        });
   
}
