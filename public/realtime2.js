/*
 * RealTime Database Firebase - PE - EA 2020
 * 0 - https://firebase.google.com/docs/database?authuser=0
 * https://firebase.google.com/docs/database/web/structure-data?authuser=0
 * ¿Base de datos NoSQL? https://es.wikipedia.org/wiki/NoSQL#:~:text=En%20inform%C3%A1tica%2C%20NoSQL%20(a%20veces,como%20lenguaje%20principal%20de%20consultas.
 * 1 - https://firebase.google.com/docs/database/web/start?authuser=0
 * Un detalle importante al momento de la creación de la base de datos. Habilitar de modo de prueba para
 * que cualquier usario puedo modificar la base de datos.
 */
var firebaseConfig = {
    apiKey: "AIzaSyC6Jrt-ERGu6zDjouQzpCZimKQRszyuq2I",
    authDomain: "proyecto-febrero-266e7.firebaseapp.com",
    databaseURL: "https://proyecto-febrero-266e7-default-rtdb.firebaseio.com/",
    projectId: "proyecto-febrero-266e7",
};

document.getElementById("boton_escritura").addEventListener('click', escribir_db, false);
/* https://firebase.google.com/docs/reference/js/firebase#functions_1
   El valor de retorno del método initializeApp es una instancia de inicialización de la aplicación.
   A partir de aquí podre emplear los servicios disponibles. emoroni */
var firebase_app = firebase.initializeApp(firebaseConfig);

// Get a reference to the database service
var database_ref = firebase.database();

// https://www.w3schools.com/jsref/met_element_addeventlistener.asp



// 2
// Create a new post reference with an auto-generated id
// https://firebase.google.com/docs/database/web/lists-of-data?authuser=0
// https://firebase.google.com/docs/database/web/lists-of-data?authuser=0#reading_and_writing_lists
// https://firebase.google.com/docs/reference/js/firebase.database.Reference#push
// ¿Qué problema existirá al guardar la información de esta manera? emoroni.

/*function escribir_db (){

  var concept = document.getElementById("concepto").value;
  var imprt = document.getElementById("importe").value;

    firebase.database().ref('Movimientos/').push({
        ie: "INGRESO",
        importe: imprt,
        concepto : concept
    });
}*/

var userId;



firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        userId = firebase.auth().currentUser.uid; // https://firebase.google.com/docs/reference/js/firebase.auth.Auth?authuser=0#currentuser
        // https://firebase.google.com/docs/reference/js/firebase.User?authuser=0#uid
        console.log("UserID: " + userId);
        // Llamo a este método una vez que efectivamente tendo el userID. Si no lo hago así se rompe la ruta. emoroni.
        document.getElementById("log").addEventListener('click', function () {firebase.auth().signOut();location.replace('./login.html')}, false);
        document.getElementById('log').innerHTML = 'Log Out';

        document.getElementById("mis-comentarios").addEventListener('click', function () {location.replace('./miscomentarios.html')}, false);
        document.getElementById('mis-comentarios').innerHTML = 'Mis Comentarios';
    } else {
        // No user is signed in.
        console.log("No hay un usuario.");
        document.getElementById("log").addEventListener('click', function () {location.replace('login.html');}, false);
        document.getElementById('log').innerHTML = 'Log In';

        document.getElementById("mis-comentarios").remove();

        
        document.getElementById("concepto").setAttribute("disabled",false);
        document.getElementById("boton_escritura").setAttribute('disabled',false);



    }
    child_added();
});

function escribir_db (){
 
    var concepto = document.getElementById("concepto").value;
    //var importe = document.getElementById("importe").value;
   
    if(concepto != '' /*&& importe != '' && !isNaN(importe)*/)
    {
      // console.log("Bandera todo OK"); // Descomentar para debbuing. emoroni
      firebase.database().ref('Comentarios/' + userId).push({
        ie: "Original",
        //importe: importe,
        concepto : concepto,
      });
    //limpio campos.
      document.getElementById("concepto").value = '';
      //document.getElementById("importe").value = '';
      alert('Carga exitosa.');
      
    }
    else if(concepto == ''/*|| importe == ''*/)
    {
      // console.log("Bandera campos"); // Descomentar para debbuging. emoroni
      alert('Complete todos los campos por favor.');
    }
    /*else if(isNaN(importe))
    {
      // console.log("Bandera isNaN"); // Descomentar para debbuging. emoroni
      alert('El campo de importe debe contener un valor numérico.');
    }    */
  }
function child_added() {
    firebase.database().ref('Comentarios/').on('child_added', function (data) {
        console.log(data.ref.path.pieces_[1])
        var usuario_actual = data.ref.path.pieces_[1];
        firebase.database().ref('Comentarios/' + usuario_actual).on('child_added', function (data) {
            console.log(data.val().concepto)

            var key = data.key;
            var comentario = data.val().concepto;

            firebase.database().ref('Datos_Usuarios/' + usuario_actual).on('child_added', function (data) {

                var del_id = key + "_del";

                //console.log(data.val().importe);
                console.log(key);
                console.log(del_id);

                // https://www.w3schools.com/jquery/default.asp
                // https://www.w3schools.com/jquery/html_append.asp
                $(columna_coleccion).append('<div class="col-sm-3 elementos_listado">' + comentario + '</div>');
                $(columna_coleccion).append('<div class="col-sm-3 elementos_listado">' + data.val().inputUsername + '</div>');
                $(columna_coleccion).append('<div class="col-sm-3 elementos_listado">' + data.val().ciudad + '</div>');
                if (userId == "UW3HBYoTadXGtLR7mMgt3yLKtoa2") {
                    $(columna_coleccion).append('<div class="col-sm-3">' + indexar_botones(key) + '</div>');

                    // Aquí existe una diferencia sustancial en la sintaxis cuando es necesario que la función invocada
                    // en el click recibe parámetros. emoroni. https://stackoverflow.com/questions/56631140/why-javascript-performs-click-event-when-i-dont-click-on-the-item
                    document.getElementById(del_id).addEventListener('click', function () {
                        delete_db('Comentarios/' + usuario_actual + '/' + key)
                    }, false);
                } else {
                    $(columna_coleccion).append('<div class="col-sm-3"></div>');
                }
            })
        })

    })
}

function indexar_botones(key) {
    var boton_borrar = '<button type="button" class="btn btn-danger boton-borrar" id="' + key + '_del">Borrar</button>';
    return boton_borrar;
}

function delete_db(path) {

    firebase.database().ref(path).remove();
    alert('Elemento eliminado.');
    location.reload();
}