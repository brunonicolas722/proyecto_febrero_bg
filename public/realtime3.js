var firebaseConfig = {
    apiKey: "AIzaSyC6Jrt-ERGu6zDjouQzpCZimKQRszyuq2I",
    authDomain: "proyecto-febrero-266e7.firebaseapp.com",
    databaseURL: "https://proyecto-febrero-266e7-default-rtdb.firebaseio.com/",
    projectId: "proyecto-febrero-266e7",
};

var firebase_app = firebase.initializeApp(firebaseConfig);

var database_ref = firebase.database();

var userId;

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        userId = firebase.auth().currentUser.uid; // https://firebase.google.com/docs/reference/js/firebase.auth.Auth?authuser=0#currentuser
        // https://firebase.google.com/docs/reference/js/firebase.User?authuser=0#uid
        console.log("UserID: " + userId);
        // Llamo a este método una vez que efectivamente tendo el userID. Si no lo hago así se rompe la ruta. emoroni.
        document.getElementById("log").addEventListener('click', function () {firebase.auth().signOut();location.replace('./login.html')}, false);
        document.getElementById('log').innerHTML = 'Log Out';

        document.getElementById("mis-comentarios").addEventListener('click', function () {location.replace('./miscomentarios.html')}, false);
        document.getElementById('mis-comentarios').innerHTML = 'Mis Comentarios';
    } else {
        // No user is signed in.
        console.log("No hay un usuario.");
        document.getElementById("log").addEventListener('click', function () {location.replace('login.html');}, false);
        document.getElementById('log').innerHTML = 'Log In';

        document.getElementById("mis-comentarios").remove();
    }
});